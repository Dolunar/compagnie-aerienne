---- Projet de Base de Données

-- Numero etudiant :  32 006 223


--  La suppression des objets / Fonctions / Procédures existants

-- Fonctions
DROP FUNCTION DureeMoyVol; -- Suppression de la fonction

-- Procédures

-- Suppresion des 2 procédures

DROP PROCEDURE Programmer;
DROP PROCEDURE VolsLiaison;

-- Tables

-- Suppression des Tables : 

DROP TABLE Equipage; 
DROP TABLE DepartVol;
DROP TABLE Vol;
DROP TABLE Personnel;

/* Sujet
Il s’agit de réaliser une interface en PL/SQL Oracle sur une base de données pour une application
permettant à une compagnie aérienne de gérer une de ses activités : « Affecter les équipages aux
vols ». Le schéma relationnel de la base de données à implémenter est le suivant :
Vol (NoVol, Depart, Arrivee)
DepartVol (NoVol, DateHeureDepart, DureeVol)
Personnel (Matricule, Nom, Categorie, Fonction)
Equipage (NoAffectation, NoVol, DateHeureDepart, Matricule)
Départ, Arrivée : Noms d’Aéroport
DuréeVol : en minutes
Catégorie : {‘Navigant’, ‘Non Navigant’} 
*/

CREATE TABLE Vol (
    NoVol VARCHAR2(5) PRIMARY KEY,  -- Numéro du vol
    Depart VARCHAR2(50) NOT NULL,   -- D'où on part
    Arrivee VARCHAR2(50) NOT NULL   -- Où on arrive
);

CREATE TABLE Personnel (
    Matricule VARCHAR2(5) PRIMARY KEY,                                          -- Matricule du personnel qu'on désigne
    Nom VARCHAR2(50) NOT NULL,                                                  -- Son Nom, Exemple : BENOIT ALTRZAZ
    Categorie VARCHAR2(50) CHECK (Categorie IN ('Navigant', 'Non Navigant')),   -- Catégorie de la personne, soit il est navigant soit il navigue aps
    Fonction VARCHAR2(50) NOT NULL                                              -- Sa fonction
);

 
CREATE TABLE DepartVol (                                            
    NoVol VARCHAR2(5) NOT NULL,                                     -- Numéro du Vol Actuel.
    DateHeureDepart DATE NOT NULL,                                  -- Date Heure et Minute de départ du vol Exemple : 05/01/2024 12:00
    DureeVol NUMBER NOT NULL CHECK (DureeVol >= 0),                 -- Durée du vol qui doit être plus grand ou égal à 0
    PRIMARY KEY (NoVol, DateHeureDepart),                           -- Les 2 clefs primaires
    FOREIGN KEY (NoVol) REFERENCES Vol(NoVol) ON DELETE CASCADE     -- Numéro du Vol supprimé = Depart supprimé
);


CREATE TABLE Equipage (
    NoAffectation VARCHAR2(5) PRIMARY KEY,                                                                  -- Où notre petit equipage est affecte
    NoVol VARCHAR2(5) NOT NULL,                                                                             -- Numero du vol
    DateHeureDepart DATE NOT NULL,                                                                          -- Date Heure et Minute de départ du vol Exemple : 05/01/2024 12:00
    Matricule VARCHAR2(5) NOT NULL,                                                                         -- Matricule du personnel qu'on désigne
    FOREIGN KEY (NoVol, DateHeureDepart) REFERENCES DepartVol(NoVol, DateHeureDepart) ON DELETE CASCADE,    -- Pour supp 
    FOREIGN KEY (Matricule) REFERENCES Personnel(Matricule) ON DELETE CASCADE                               -- Même chose
);

-- FONCTION :

--Exercice 2
/* 
DureeMoyVol(noVol in varchar2) return number
Cette fonction calcule et retourne la durée moyenne d’un vol en heures. Si le vol n’existe
pas, la fonction retourne -1, et si le vol n’est pas programmé, la fonction retourne 0. 
*/

CREATE OR REPLACE FUNCTION DureeMoyVol(NumVol IN VARCHAR2) RETURN NUMBER AS --Mis NumVol pour pas qu'il y se passe quelque chose de mal
    DurMoy NUMBER;

BEGIN
    SELECT AVG(DureeVol) INTO DurMoy -- on initialise la dureemoyenne grâce à la moyenne de la durée de tous les vols dans NoVol
    FROM DepartVol
    WHERE NoVol = NumVol;

    IF DurMoy IS NULL THEN
        DBMS_OUTPUT.PUT_LINE('Durée vol ' || NumVol || ' = 0 (Vol non programmé)'); -- Affichage pour le test si DurMoy est NULL
        RETURN 0; -- Vol non programmé
    ELSE
        DBMS_OUTPUT.PUT_LINE('Durée vol ' || NumVol || ' = ' || TO_CHAR(DurMoy / 60, 'FM99990.99') || ' heures'); -- Affichage pour le test
        RETURN DurMoy / 60;
    END IF;
END DureeMoyVol;
/


-- Procédures :

/* Exercice 1 :

Programmer(noVol in varchar2, DateHeureDep in date, DV in number)
Cette procédure ajoute/modifie/supprime un départ pour un vol (noVol) à une date de
départ (DateHeureDep) avec une durée de vol (DV) :
- DateHeureDep > date et heure courante ;
- DV ne peut pas être NULL ;
- Si le départ de vol n’existe pas et DV est positif, le départ de vol est ajouté ;
- Si le départ de vol existe et DV est positif, DureeVol est remplacée par DV ;
- Si le départ de vol existe et DV = 0, le départ de vol est supprimé et son équipage
annulé ; 

*/

CREATE OR REPLACE PROCEDURE Programmer(NumVol IN VARCHAR2, DateHeureDep IN DATE, DureeVol IN NUMBER) 
IS
    OuiVol NUMBER(2); -- Est-ce qui désigne si le Vol marche (OU PAS)
    
BEGIN
    IF DateHeureDep <= SYSDATE THEN
        RAISE_APPLICATION_ERROR(-1, 'Le départ doit être plus grand que la date courante'); -- Gestion de l'erreurrrrrrr
        RETURN;
        
    ELSIF DureeVol IS NULL THEN -- Gestion de l'erreur
        RAISE_APPLICATION_ERROR(-2, 'DV ne peut pas être nul en fait c est pas logique svp'); -- Gestion de l'erreurrrrrr
        RETURN;
        
    END IF;

    -- On regarde si le Vol est présent
    SELECT COUNT(*) -- On prend tout et l'assigne dans OuiVol pour voir si c'est la même chose ou pas
    INTO OuiVol
    FROM DepartVol
    WHERE NoVol = NumVol AND DateHeureDepart = DateHeureDep;

    
    IF OuiVol = 0 AND DureeVol > 0 THEN
         -- Si le départ de vol n'existe pas et DureeVol est positif, ajout du départ de vol
        INSERT INTO DepartVol(NoVol, DateHeureDepart, DureeVol) 
        VALUES (NumVol, DateHeureDep, DureeVol);
        
        
    ELSIF OuiVol = 1 AND DureeVol > 0 THEN
        -- Si le départ de vol existe et DureeVol est positif, mise à jour de DureeVol
        UPDATE DepartVol
        SET DureeVol = DureeVol
        WHERE NoVol = NumVol AND DateHeureDepart = DateHeureDep;
        
        
    ELSIF OuiVol = 1 AND DureeVol = 0 THEN
        -- Si DureeVol = 0, suppression du départ de vol et annulation de l'équipage
        DELETE FROM Equipage
        WHERE NoVol = NumVol AND DateHeureDepart = DateHeureDep; -- suppression equipage
        DELETE FROM DepartVol
        WHERE NoVol = NumVol AND DateHeureDepart = DateHeureDep; -- Suppression du vol du coup
    END IF;

    COMMIT; -- WE ARE COMMITING
    
EXCEPTION 
    WHEN OTHERS THEN
        ROLLBACK;
        RAISE;
END Programmer;
/






/* Exercice 3 :

VolsLiaison(Depart in varchar2, Arrivee in varchar2)
Cette procédure recherche et affiche, pour une liaison donnée par le couple (Depart, Arrivee),
le NoVol des vols correspondants ainsi la durée moyenne des vols programmés (calculée
par la fonction DureeMoyVol). Si pas de vol, on affiche le message « Pas de vol pour cette
liaison ». 

*/
CREATE OR REPLACE PROCEDURE VolsLiaison(DepartVol IN VARCHAR2, ArriveeVol IN VARCHAR2) AS
    NumVol2 VARCHAR2(10);
    DurMoy2 NUMBER;

BEGIN
    -- Je recherche des vols correspondants à la liaison
    FOR rec IN (SELECT V.NoVol, DureeMoyVol(V.NoVol) AS DurMoy
                FROM Vol V
                WHERE V.Depart = DepartVol AND V.Arrivee = ArriveeVol)
    LOOP
        NumVol2 := rec.NoVol;
        DurMoy2 := rec.DurMoy;

        -- On affiche les résultes
        DBMS_OUTPUT.PUT_LINE('NoVol : ' || NumVol2 || ', DureeMoyenne : ' || DurMoy2 || ' Heures');
    END LOOP;

    -- Affichage du message personnalisé par moi-même s'il n'y a pas de vol
    IF NumVol2 IS NULL THEN
        DBMS_OUTPUT.PUT_LINE('Pas de vol pour cette liaison, je suis désolé :( ');
    END IF;
END VolsLiaison;
/